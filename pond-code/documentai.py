from google.cloud import documentai_v1beta2 as documentai
import json

def main(project_id='YOUR_PROJECT_ID',
         input_uri='gs://cloud-samples-data/documentai/invoice.pdf'):
    """Process a single document with the Document AI API, including
    text extraction and entity extraction."""

    client = documentai.DocumentUnderstandingServiceClient()

    gcs_source = documentai.types.GcsSource(uri=input_uri)

    # mime_type can be application/pdf, image/tiff,
    # and image/gif, or application/json
    # mime_type = 'image/tiff'
    mime_type = 'application/pdf'
    input_config = documentai.types.InputConfig(
        gcs_source=gcs_source, mime_type=mime_type)

    # Location can be 'us' or 'eu'
    parent = 'projects/{}/locations/us'.format(project_id)
    request = documentai.types.ProcessDocumentRequest(
        # document_type='general',
        parent=parent,
        input_config=input_config,
        ocr_params=documentai.OcrParams(language_hints=['th-TH'])
        )

    document = client.process_document(request=request)
    print('Document Text: {}'.format(document.text))
    result = {
        'text': document.text,
        'entities': []
    }
    # print(document.content)
    # print(dir(document))
    # print(document.text_styles)
    # print(document.translations)
    # All text extracted from the document
    # print('Document Text: {}'.format(document.text))
    # print(document.entities)
    

    def _get_text(el):
        """Convert text offset indexes into text snippets.
        """
        response = ''
        # If a text segment spans several lines, it will
        # be stored in different text segments.
        for segment in el.text_anchor.text_segments:
            start_index = segment.start_index
            end_index = segment.end_index
            response += document.text[start_index:end_index]
        return response

    for entity in document.entities:
        # print('Entity type: {}'.format(entity.type))
        # print('Text: {}'.format(_get_text(entity)))
        # print('Mention text: {}\n'.format(entity.mention_text))
        result['entities'].append(entity.mention_text)
    
    with open('documentai.json', 'w') as outfile:
        json.dump(result, outfile)


# main(project_id='credit-ok-testing')
# main(project_id='credit-ok-testing', input_uri='gs://datathon-bkk-dreams/0001.tiff')
main(project_id='credit-ok-testing', input_uri='gs://datathon-bkk-dreams/2563 กทม.-5.pdf')
# main(project_id='credit-ok-testing', input_uri='gs://datathon-bkk-dreams/0001-converted.pdf')