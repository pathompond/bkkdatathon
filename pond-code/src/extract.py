from google.cloud import documentai_v1beta2 as documentai
from google.cloud import vision
from google.cloud import storage
from google.protobuf import json_format
import re
import json


def get_text_from_document_ai(input_uri):
    # return [
    #     "2",
    #     "(1) งบกลาง รวม 14,878,887,438 บาท",
    #     "1. เงนส ารองจายทวไป 486,536,500 บาท",
    #     "1.1 กรณฉกเฉนหรอจ าเปน 100,000,000 บาท",
    #     "1.2 กรณคาใชจายตาง ๆ เกยวกบน าทวม 100,000,000 บาท",
    #     "1.3 กรณคาใชจายเพอการพฒนา",
    #     "กรงเทพมหานคร 286,536,500 บาท",
    #     "2. เงนชวยเหลอขาราชการและลกจาง 2,700,000,000 บาท",
    #     "3. เงนบาเหนจลกจาง 650,000,000 บาท",
    #     "4. คาตดตงไฟฟาสาธารณะ 200,000,000 บาท",
    #     "5. เงนส ารองส าหรบคางาน สวนทเพมตามสญญา แบบปรบราคาได 20,000,000 บาท",
    #     "6. เงนส ารองส าหรบคาใชจายเกยวกบบคลากร 300,000,000 บาท",
    #     "7. คาใชจายเกยวกบภารกจและ หรอนโยบายทไดรบมอบจากรฐบาล 4,395,109,000 บาท",
    #     "8. เงนรางวลและเงนชวยเหลอส าหรบผลการปฏบตราชการประจ าป 2,000,000,000 บาท",
    #     "9. เงนส ารองส าหรบภาระผกพนทคางจายตามกฎหมาย 1,555,873,938 บาท",
    #     "10. เงนชวยคาครองชพผไดรบบานาญของกรงเทพมหานคร 218,000,000 บาท",
    #     "11. เงนส ารองส าหรบจายเปนเงนบาเหนจบานาญ",
    #     "ขาราชการกรงเทพมหานคร 2,353,368,000 บาท",
    #     ""
    # ]
    project_id = 'credit-ok-testing'
    client = documentai.DocumentUnderstandingServiceClient()

    gcs_source = documentai.types.GcsSource(uri=input_uri)

    # mime_type can be application/pdf, image/tiff,
    # mime_type = 'image/tiff' # return polian text
    mime_type = 'application/pdf' # more exfective
    input_config = documentai.types.InputConfig(
        gcs_source=gcs_source, mime_type=mime_type)

    # Location can be 'us' or 'eu'
    parent = 'projects/{}/locations/us'.format(project_id)
    request = documentai.types.ProcessDocumentRequest(
        # document_type='general', #Not help
        parent=parent,
        input_config=input_config,
        # ocr_params=documentai.OcrParams(language_hints=['th-TH']) #Not help
        )

    document = client.process_document(request=request)
    text = document.text
    return text.split('\n')

def get_text_from_cloud_vision(gcs_source_uri, gcs_destination_uri):
    # return ['2', '(1)', 'รวม', '14,878,887,438 บาท', '486,536,500 บาท', '2,700,000,000 บาท', '650,000,000 บาท', 'งบกลาง', '1. เงินสำรองจ่ายทั่วไป', '1.1 กรณีฉุกเฉินหรือจำเป็น', '100,000,000 บาท', '1.2 กรณีค่าใช้จ่ายต่าง ๆ เกี่ยวกับน้ำท่วม', '100,000,000 บาท', '1.3 กรณีค่าใช้จ่ายเพื่อการพัฒนา', 'กรุงเทพมหานคร', '286,536,500 บาท', '2. เงินช่วยเหลือข้าราชการและลูกจ้าง', '3. เงินบำเหน็จลูกจ้าง', '4. ค่าติดตั้งไฟฟ้าสาธารณะ', '5. เงินสำรองสำหรับค่างาน ส่วนที่เพิ่มตามสัญญา แบบปรับราคาได้', '6. เงินสำรองสำหรับค่าใช้จ่ายเกี่ยวกับบุคลากร', '7. ค่าใช้จ่ายเกี่ยวกับภารกิจและ หรือนโยบายที่ได้รับมอบจากรัฐบาล', '8. เงินรางวัลและเงินช่วยเหลือสำหรับผลการปฏิบัติราชการประจำปี', '9. เงินสำรองสำหรับภาระผูกพันที่ค้างจ่ายตามกฎหมาย', '10. เงินช่วยค่าครองชีพผู้ได้รับบำนาญของกรุงเทพมหานคร', '11. เงินสำรองสำหรับจ่ายเป็นเงินบำเหน็จบำนาญ', 'ข้าราชการกรุงเทพมหานคร', '200,000,000 บาท', '20,000,000 บาท', '300,000,000 บาท', '4,395,109,000 บาท', '2,000,000,000 บาท', '1,555,873,938 บาท', '218,000,000 บาท', '2,353,368,000 บาท', '']
    mime_type = 'image/tiff' # better than application/json

    # How many pages should be grouped into each json output file.
    batch_size = 2

    client = vision.ImageAnnotatorClient()

    feature = vision.types.Feature(
        type=vision.enums.Feature.Type.DOCUMENT_TEXT_DETECTION)

    gcs_source = vision.types.GcsSource(uri=gcs_source_uri)
    input_config = vision.types.InputConfig(
        gcs_source=gcs_source, mime_type=mime_type)

    gcs_destination = vision.types.GcsDestination(uri=gcs_destination_uri)
    output_config = vision.types.OutputConfig(
        gcs_destination=gcs_destination, batch_size=batch_size)

    image_context = vision.types.ImageContext(language_hints=['th'])
    async_request = vision.types.AsyncAnnotateFileRequest(
        features=[feature], 
        image_context=image_context,
        input_config=input_config,
        output_config=output_config)

    operation = client.async_batch_annotate_files(
        requests=[async_request])

    print('Waiting for the operation to finish.')
    operation.result(timeout=420)

    # Once the request has completed and the output has been
    # written to GCS, we can list all the output files.
    storage_client = storage.Client()

    match = re.match(r'gs://([^/]+)/(.+)', gcs_destination_uri)
    bucket_name = match.group(1)
    prefix = match.group(2)

    bucket = storage_client.get_bucket(bucket_name)

    # List objects with the given prefix.
    blob_list = list(bucket.list_blobs(prefix=prefix))
    print('Output files:')
    for blob in blob_list:
        print(blob.name)
    output = blob_list[0]

    json_string = output.download_as_string()
    response = json_format.Parse(
        json_string, vision.types.AnnotateFileResponse())
    first_page_response = response.responses[0]
    annotation = first_page_response.full_text_annotation
    return annotation.text.split('\n')

def get_reg_str(mapping_str):
    reg_str = r'^'
    for char in mapping_str:
        if char == 'ํ' or char == 'ำ':
            char = '[ าำ]+'
        reg_str = reg_str + f'({char})*'
    return reg_str + ''
    # return r'['+mapping_str+'\s]+'


def map_str_word(mapping_strs, word):
    word = word.strip()
    word_reg_str = get_reg_str(word)
    # print('word', word)
    # print('word_reg_str', word_reg_str)
    for map_str in mapping_strs:
        map_reg_str = get_reg_str(map_str)
        # print('map_reg_str', map_reg_str)
        match = re.match(word_reg_str, map_str)
        # print(match)
        if match and len(match.group()) >= len(word):
            # print('1:', map_str, word_reg_str)
            return word.replace(match.group(), map_str), True
        match_2 = re.match(map_reg_str, word)
        # print(match_2)
        if match_2 and len(match_2.group()) >= len(word):
            # print('2:', word, map_reg_str)
            return word.replace(match_2.group(), map_str), True
    # print('3: no match')
    return word, False

def map_str_label(mapping_strs, label):
    # print('label >>', label)
    label_result, can_map = map_str_word(mapping_strs, label)
    if not can_map:
        label = label.replace(' า', 'า')
        words = label.split(' ')
        result_word = []
        for word in words:
            word_mapped, can_map_word = map_str_word(mapping_strs, word)
            result_word.append(word_mapped)
        label_result = ' '.join(result_word)
    return label_result



def structure_item(text, mapping_strs=[]):
    match = find_record_item(text)
    group = ''
    if match: 
        group = match.group().strip()

    label = text.replace(group, '')
    budget_match = re.findall(r'[0-9,]+\s+บาท', label)
    budget = None
    if budget_match:
        for budget_match_item in budget_match:
            label = label.replace(budget_match_item, '')
        budget = budget_match[0].replace('บาท', '').replace(',', '').strip()
        budget = float(budget)
    label = label.strip()
    return {
        'label': map_str_label(mapping_strs, label),
        'budget': budget
    }


def find_record_item(text):
    match = re.match(r'^[0-9.]+\s+', text)
    if not match:
        return
    return match


def find_record_section(text):
    match = re.match(r'^\([0-9.]+\)\s*', text)
    if not match:
        return
    return match

def find_attrs(text, current_attrs=[], mapping_strs=[]):
    match_item = find_record_section(text)
    if match_item:
        current_attrs = []
        return False, current_attrs
    match_item = find_record_item(text)
    if match_item:
        return False, current_attrs

    attrs = []
    upkeep = re.match(r'^[รายจ่ายประจำ\s]+\s+', text)
    if upkeep:
        if current_attrs:
            current_attrs = []
        current_attrs.append('รายจ่ายประจำ')
        return True, current_attrs
    special = re.match(r'^[รายจ่ายพิเศษ\s]+\s+', text)
    if special:
        if current_attrs:
            current_attrs = []
        current_attrs.append('รายจ่ายพิเศษ')
        return True, current_attrs

    another = re.match(r'^[ก-ฮ]\.\s+', text)
    if another:
        attr = text.replace(another.group(), '')
        budget = re.search(r'[0-9,]+\s+บาท', attr)
        if not budget:
            return False, current_attrs
        start = budget.start()
        attr = attr[:start]
        if len(current_attrs) > 1:
            current_attrs.pop()
        attr = map_str_label(mapping_strs, attr)
        current_attrs.append(attr)
        return True, current_attrs

    return False, current_attrs

def get_section_label(text):
    match = re.search(r'รวม\s+[0-9,]+\s+บาท', text)
    if match:
        start = match.start()
        text = text[:start]

    match = re.match(r'\([0-9\s]+\)', text)
    if match:
        text = text.replace(match.group(), '')

    return text.strip()

def get_text_item_lines(lines, mapping_strs=[]):
    records = {}
    line_item_number = None
    current_district = None
    current_attrs = []
    is_previous_attr = False
    parent_detail = None
    is_previous_detail_item = False
    for line in lines:
        # print('line',line)
        is_attr, current_attrs = find_attrs(line, current_attrs, mapping_strs)
        if is_attr:
            # print('is_attr', line, current_attrs)
            is_previous_attr = True
            continue
    
        section = find_record_section(line)
        if section:
            current_district = section.group().strip()
            section_label = get_section_label(line)
            records[current_district] = {
                'label': map_str_label(mapping_strs, section_label),
                'items': {},
                'district': current_district
            }
            continue
        if not current_district:
            continue
        # print('current_district', current_district)
        record = find_record_item(line)
        # print('record', record)
        # collect line
        # print('current_attrs', current_attrs)
        if record:
            line_item_number = record.group().strip()
            line_item_number = line_item_number.replace('', '').strip()
            # print('parent_detail', parent_detail, line_item_number, line_item_number.split('.'))
            line_item_number_split = line_item_number.split('.')
            # print(len(line_item_number_split), line_item_number_split[-1] != '')
            if len(line_item_number_split) > 1 and line_item_number_split[-1] != '':
                topic = line_item_number_split[0]
                # print(topic, parent_detail)
                if f'{topic}.' == parent_detail:
                    records[current_district]['items'][parent_detail]['detail'].append({
                        'label': line,
                        'attrs': current_attrs.copy(),
                        'detail': []
                    })
                    # print(records)
                    is_previous_detail_item = True
                    continue
            is_previous_detail_item = False
            records[current_district]['items'][line_item_number] = {
                'label': line,
                'attrs': current_attrs.copy(),
                'detail': []
            }
            if len(line_item_number_split) <= 1 or (len(line_item_number_split) > 1 and line_item_number_split[-1] == ''):
                parent_detail = line_item_number
        elif is_previous_attr:
            line_item_number = '-'.join(current_attrs)
            records[current_district]['items'][line_item_number] = {
                'label': line,
                'attrs': current_attrs.copy(),
                'detail': []
            }
        else:
            # print('line', line)
            if not line_item_number:
                continue
            if is_previous_detail_item:
                records[current_district]['items'][parent_detail]['detail'][-1]['label'] = records[current_district]['items'][parent_detail]['detail'][-1]['label'] + line
                continue

            budget_match = re.search(r'[0-9,]+\s+บาท$', line)
            if budget_match:
                records[current_district]['items'][parent_detail]['detail'].append({
                    'label': line,
                    'attrs': current_attrs.copy(),
                    'detail': []
                })
                continue
            records[current_district]['items'][line_item_number]['label'] = records[current_district]['items'][line_item_number]['label'] + ' ' + line

        if not is_attr:
            is_previous_attr = False

    # map label - budget
    result = []
    for s_index, section in records.items():
        district = section['label']
        for r_index, record in section['items'].items():
            structure_item_result = structure_item(record['label'], mapping_strs)
            structure_item_result['attrs'] = record['attrs']
            structure_item_result['district'] = district

            result_detail = []
            for detail in record['detail']:
                _detail = structure_item(detail['label'], mapping_strs)
                _detail['attrs'] = detail['attrs']
                result_detail.append(_detail)

            structure_item_result['detail'] = result_detail


            result.append(structure_item_result)
    return result

def mapping_text_record(trecords_from_documentai, records_from_cloud_vision):
    records = {}
    for s_index, section in trecords_from_documentai.items():
        items = {}

        if not s_index in records_from_cloud_vision:
            continue

        label = section['label']

        for r_index, record in section['items'].items():
            items[r_index] = record
            
            if not r_index in records_from_cloud_vision[s_index]['items']:
                continue

            items[r_index]['label'] = records_from_cloud_vision[s_index]['items'][r_index]['label']
        
        
        records[s_index] = {
            'label': label,
            'items': items
        }
    return records

def collect_text_map(lines):
    result = []
    for line in lines:
        # page number
        match = re.match(r'[0-9]{2}$', line)
        if match:
            continue
        # start with currency
        match = re.match(r'[0-9,]+\s+บาท', line)
        if match:
            continue
        
        # remove list index
        match = re.match(r'^[ก-ฮ]\.\s+', line)
        if match:
            line = line.replace(match.group(), '')

        match = re.match(r'^[0-9.]+\s+', line)
        if match:
            line = line.replace(match.group(), '')
        
        line = get_section_label(line)
        if line == '':
            continue
        result.append(line)

    return result






