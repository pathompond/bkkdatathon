from extract import (get_text_from_document_ai, get_text_item_lines, get_text_from_cloud_vision, mapping_text_record, collect_text_map)

def get_records(uri_src_tiff, uri_src_pdf, uri_dis_pdf):
    text_from_cloud_vision = get_text_from_cloud_vision(uri_src_pdf, uri_dis_pdf)
    text_map = collect_text_map(text_from_cloud_vision)
    line_items = get_text_from_document_ai(uri_src_tiff)
    records_from_documentai = get_text_item_lines(line_items, mapping_strs=text_map)
    print(records_from_documentai)
    

# Page 5
uri_src_tiff = 'gs://datathon-bkk-dreams/2563 กทม.-5.pdf'
uri_src_pdf = 'gs://datathon-bkk-dreams/0001.tiff'
uri_dis_pdf = 'gs://datathon-bkk-dreams/0001.tiff/'

# Page 75
# uri_src_tiff = 'gs://datathon-bkk-dreams/2563 กทม.-75.pdf'
# uri_src_pdf = 'gs://datathon-bkk-dreams/2563 กทม.-75.tiff'
# uri_dis_pdf = 'gs://datathon-bkk-dreams/2563 กทม.-75.tiff/'

get_records(uri_src_tiff, uri_src_pdf, uri_dis_pdf)